# README #

NBidi - bi-directional native C# support with Unity-specific, minor,  modifications.
These modifications are based on the original NBidi library, Copyright (C) 2007-2009 Itai Bar-Haim, found in: https://sourceforge.net/projects/nbidi/

This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
